from setuptools import setup, find_packages

setup(
    namespace_packages=['bennellickeng'],
    name='bennellickeng.kitdrivers',
    version='0.0.1',
    description='Bennellick Engineering Kit Drivers',
    author='Bennellick Engineering Limited',
    author_email='info@bennellick.com',
    license='BSD 3-clause',
    packages=find_packages(),
    entry_points = {
        'console_scripts': [
            'bel-scopegrab=bennellickeng.kitdrivers.usbtmc.scope:grab_CLI'
        ]
    },
    install_requires=[
        'boltons>=17.0.0',
        'numpy',
        'scikit-rf>=0.14.0',
        'matplotlib',
    ],
)
